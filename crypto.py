import numpy as np 
import time
import pandas as pd
import tensorflow as tf
import matplotlib
import matplotlib.pyplot as plt
from dateutil.parser import parse
import datetime
from datetime import timedelta
import json
import collections
import os
import ccxt 
#bitcoin_info = pd.read_html("https://coinmarketcap.com/currencies/bitcoin/
#historical-data/?start=20130428&end="+time.strftime("%Y%m%d"))[0]
class Crypto:

    def __init__(self,symbol='bitcoin',config_file = 'config.json'):
        self.symbol = symbol
        self.configs = json.loads(open(os.path.join(os.path.dirname(__file__), config_file)).read())
        
    def __interpolate(self,x,y,size):
        fit = np.polyfit(x, y, 1)
        fit_fn = np.poly1d(fit) 
        values = fit_fn(size[1:-1])
        return values

    def __load_crypto(self, symbol, end):
        print("Loading "+symbol+" OHLCV Table\n")
        crypto_ohlcv = pd.read_html("https://coinmarketcap.com/currencies/"+symbol+"/historical-data/?start=20130428&end="+end)[0]
        #print(crypto_ohlcv)
        crypto_ohlcv = crypto_ohlcv.assign(Date=pd.to_datetime(crypto_ohlcv['Date']))
       
        crypto_ohlcv['Volume'] = (pd.to_numeric(crypto_ohlcv['Volume'], errors='coerce').fillna(0))    
        #crypto_ohlcv.loc[crypto_ohlcv['Volume']=="-",'Volume'] = 0
        #crypto_ohlcv['Volume'] = crypto_ohlcv['Volume'].astype('int64')
        crypto_ohlcv = crypto_ohlcv.sort_values(by = ['Date'])
        crypto_ohlcv['Date'] = crypto_ohlcv['Date'].dt.to_period('d')
        crypto_ohlcv.set_index('Date', inplace=True)
        print(crypto_ohlcv.head())
        print(crypto_ohlcv.tail())
        sequence_cols = ['Open*', 'Close**','Volume','High','Low']
        crypto_ohlcv = crypto_ohlcv[sequence_cols]
        crypto_ohlcv = crypto_ohlcv.rename(index=str,columns={"Open*": "Open", "Close**": "Close"}) 
        #crypto_ohlcv.index = pd.RangeIndex(len(crypto_ohlcv.index))
        print(crypto_ohlcv.head())
        print(crypto_ohlcv.tail())
        return crypto_ohlcv

    def __load_sentiment(self, symbol):
        print("Loading "+symbol+" Sentiment Table\n")
        sentiment_table = pd.read_csv('./databases/'+symbol+'_sent.csv', index_col=0)
        sequence_cols = ['Sentiment']#'High_Sent','Low_Sent']
        sentiment_table = sentiment_table[sequence_cols]
        print(sentiment_table.head())
        print(sentiment_table.tail())
        return sentiment_table

    def __plot_show(self, table):
        fig, (ax1, ax2) = plt.subplots(2,1, gridspec_kw = {'height_ratios':[3, 1]})
        ax1.set_ylabel('Closing Price ($)',fontsize=12)
        ax2.set_ylabel('Volume ($ bn)',fontsize=12)
        ax2.set_yticks([int('%d000000000'%i) for i in range(10)])
        ax2.set_yticklabels(range(10))
        ax1.set_xticks([datetime.date(i,j,1) for i in range(2016,2019) for j in [1,7]])
        ax1.set_xticklabels('')
        ax2.set_xticks([datetime.date(i,j,1) for i in range(2016,2019) for j in [1,7]])
        ax2.set_xticklabels([datetime.date(i,j,1).strftime('%b %Y')  for i in range(2016,2019) for j in [1,7]])
        ax1.plot(table.index.astype(datetime.datetime),table['Close'])
        ax2.bar(table.index.astype(datetime.datetime).values, table['Volume'].values)
        fig.tight_layout()
        plt.show()

    def sent_data_clean(self):
        #Cleans the databae-sent.csv interpolate data, erase buzz and others columns and save the
        #data in another file with the name {symbol}_sent.csv

        sent_data = pd.read_csv('./databases/'+self.symbol+'-sent.csv')
        rm_cols = set(sent_data.columns) - set(['timestamp','sentiment','low','high','volume'])    
        
        for col in rm_cols:
            del sent_data[col]
        
        sent_data = sent_data.fillna(0)
        for row in sent_data.itertuples():
            sent_data.at[row.Index,'timestamp'] = datetime.datetime.utcfromtimestamp(row.timestamp)
        sent_data = sent_data.sort_values(by=['timestamp'])
        sent_data = sent_data.loc[sent_data['timestamp'].dt.to_period('d').drop_duplicates().index]


        if(self.symbol == 'bitcoin'):
            start_time = '2013/04/28'
            
        elif(self.symbol == 'ethereum'):
            start_time = '2015/08/07'
            
        sent_data = sent_data.loc[sent_data['timestamp'] >= datetime.datetime.strptime(start_time, "%Y/%m/%d")]#Ethereum
        zeros_index = list()
        flag = 0
        #print(sent_data)
        for i, data in sent_data.iterrows():
            if data.volume == 0.0:
                zeros_index.append(i)
                flag += 1
            else:
                flag = 0

            if(flag == 0 and len(zeros_index) > 0):
                x_ = np.arange(len(zeros_index) + 2)
                x = [x_[0], x_[-1]]
                y_h = [sent_data.high[zeros_index[0] + 1], sent_data.high[zeros_index[-1] - 1]]
                y_l = [sent_data.low[zeros_index[0] + 1], sent_data.low[zeros_index[-1] - 1]]
                val_h = self.__interpolate(x,y_h,x_)
                val_l = self.__interpolate(x,y_l,x_)
                sent_data.loc[zeros_index,'high'] = val_h
                sent_data.loc[zeros_index,'low'] = val_l
                sent_data.loc[zeros_index,'sentiment'] = np.mean([val_h,val_l],axis = 0)
                zeros_index.clear()
        sent_data = sent_data.rename(index=str,
        columns={"timestamp": "Date", "volume": "Volume_Sent","high":"High_Sent","low":"Low_Sent","sentiment":"Sentiment"})
        sent_data['Date'] = sent_data['Date'].dt.to_period('d')
        sent_data.set_index('Date', inplace=True)
        print('Sentiment table for '+self.symbol+' created from '+ str(sent_data.index[0]))
        print(sent_data.head())
        print(sent_data.tail())
        sent_data.to_csv('./databases/'+self.symbol+'_sent.csv')    

    def __add_volatility(self, data):
        kwargs = {'Close_off_high': lambda x: 2*(x['High']- x['Close'])/(x['High']-x['Low'])-1,
        'Volatility_HLO': lambda x: (x['High']- x['Low'])/(x['Open']),
        'Volatility': lambda x: x['Close'].rolling(self.configs['data']['x_window_size']).std(),
        'Mean': lambda x: x['Close'].rolling(self.configs['data']['x_window_size']).mean()}
        data = data.assign(**kwargs).fillna(0)
        return data

    
    def merge_tables(self):
        # merge sentiment and OHLCV tables to return a single table with Date, Volume,
        #HighSent, Low_Sent, Open and Close
        crypto_sent_info = self.__load_sentiment(symbol = self.symbol)
        end = pd.to_datetime(crypto_sent_info.index[-1]).strftime("%Y%m%d")
        crypto_info = self.__load_crypto(symbol = self.symbol,end=end)
        crypto_info = self.__add_volatility(crypto_info)
        merge_table = pd.merge(crypto_sent_info, crypto_info, how = 'inner',left_index=True, right_index=True)
        merge_table = merge_table.fillna(0)
        merge_table = merge_table.loc[merge_table['Volume']!=0]
        merge_table = merge_table.loc[merge_table['Volatility']!=0]
        merge_table.to_csv('./databases/'+self.symbol+'.csv')
        print("Tables  merged")
        print(merge_table.head())
        print(merge_table.tail())
        print("\n Tables for "+self.symbol+" merged in databases/"+self.symbol+".csv\n")
        #self.__plot_show(merge_table)
        return(merge_table)

    ##CLEAN FILE OHLCV usually for 5 min databases generated by hand with utils ipynb
    def clean_file(self,file, save_file):
        PATH = './databases/'+file
        df = pd.read_csv(PATH, index_col = 0)
        df = self.__add_volatility(df)
        df = df.loc[df['Volume']!=0]
        df = df.loc[df['Volatility']!=0]
        df.index = pd.RangeIndex(len(df.index))
        df.to_csv('./databases/'+save_file, index = False)


class Exchanges:

    def __init__(self, df1, symbol1, df2, symbol2):
        self.df1 = df1
        self.sym1 = symbol1
        self.df2 = df2
        self.sym2 = symbol2

    def merge(self, save_file = 'exchanges.csv'):
        self.exchanges = pd.merge(self.df1, self.df2, how = 'inner',
                        suffixes=['_'+self.sym1,'_'+self.sym2],left_index=True,
                        right_index=True)
        self.exchanges = self.exchanges.reindex_axis(sorted(self.exchanges.columns), axis=1)

        print(self.exchanges.head())
        print(self.exchanges.tail())
        print('Dataframe saved in /databases/'+save_file)
        self.exchanges.to_csv('./databases/'+save_file)