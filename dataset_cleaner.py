import h5py
import numpy as np
import pandas as pd

class DatasetClean:
	"""Extract Transform Load class for all data operations pre model inputs. Data is read in generative way to allow for large datafiles and low memory utilisation"""

	def generate_clean_data(self, filename, batch_size=1000, start_index=0, split = 0.7, train = False, val = False, test=False):
		with h5py.File(filename, 'r') as hf:
			data_len = hf['x'].shape[0]
			
			if train: 
				end = int(start_index + (data_len - start_index) * split)
			
			elif val: 
				start_index = int(start_index + (data_len - start_index) * split)
				end = data_len

			elif test:
				start_index = int(data_len - batch_size)
				end = data_len

			i = start_index

			while True:
				#if (i + batch_size > end): i = end - batch_size 
				data_x = hf['x'][i:i+batch_size]
				data_y = hf['y'][i:i+batch_size]
				data_mm = hf['mm'][i:i+batch_size]
				#print(i)
				if(i + batch_size < end):
					i += batch_size
				else:
					i = start_index
				yield (data_x, data_y, data_mm)

	def create_clean_datafile(self, filename_in, filename_out, batch_size=1000, x_window_size=100, y_window_size=1, y_col=0, filter_cols=None, normalise=True):
		"""Incrementally save a datafile of clean data ready for loading straight into model"""
		print('> Creating x & y data files...')

		data_gen = self.clean_data(
			filepath = filename_in,
			batch_size = batch_size,
			x_window_size = x_window_size,
			y_window_size = y_window_size,
			y_col = y_col,
			filter_cols = filter_cols,
			normalise = normalise
		)

		i = 0
		with h5py.File(filename_out, 'w') as hf:
			x1, y1, mm1 = next(data_gen)
			#Initialise hdf5 x, y datasets with first chunk of data
			print(mm1.shape, x1.shape)
			rcount_x = x1.shape[0]
			dset_x = hf.create_dataset('x', shape=x1.shape, maxshape=(None, x1.shape[1], x1.shape[2]), chunks=True)
			dset_x[:] = x1
			rcount_y = y1.shape[0]
			dset_y = hf.create_dataset('y', shape=y1.shape, maxshape=(None,y1.shape[1], y1.shape[2]), chunks=True)
			dset_y[:] = y1
			rcount_mm = mm1.shape[0]
			dset_mm = hf.create_dataset('mm', shape=mm1.shape, maxshape=(None,mm1.shape[1], mm1.shape[2]), chunks=True)
			dset_mm[:] = mm1

			for x_batch, y_batch, mm_batch in data_gen:
				#Append batches to x, y hdf5 datasets
				print('> Creating x & y & min_max data files | Batch:', i, end='\r')
				dset_x.resize(rcount_x + x_batch.shape[0], axis=0)
				dset_x[rcount_x:] = x_batch
				rcount_x += x_batch.shape[0]
				dset_y.resize(rcount_y + y_batch.shape[0], axis=0)
				dset_y[rcount_y:] = y_batch
				rcount_y += y_batch.shape[0]
				dset_mm.resize(rcount_mm + mm_batch.shape[0], axis=0)
				dset_mm[rcount_mm:] = mm_batch
				rcount_mm += mm_batch.shape[0]
				i += 1

		print('> Clean datasets created in file `' + filename_out + '.h5`')


	def clean_data(self, filepath, batch_size, x_window_size, y_window_size, y_col, filter_cols, normalise):
		"""Cleans and Normalises the data in batches `batch_size` at a time"""
		data = pd.read_csv(filepath, index_col=0)

		if(filter_cols):
			#Remove any columns from data that we don't need by getting the difference between cols and filter list
			rm_cols = set(data.columns) - set(filter_cols)
			for col in rm_cols:
				del data[col]
		data = data.reindex_axis(sorted(data.columns), axis=1)
		#Convert y-predict column name to numerical index
		#y_col = list(data.columns).index(y_col)
		y_col = sorted(y_col)
		num_rows = len(data)
		x_data = []
		y_data = []
		min_max_data = []
		mean_std = False
		zero_std = False
		min_max_norm = True
		i = 0
		print(data.head())

		while((i+x_window_size+y_window_size) <= num_rows):

			x_window_data = data[i:(i+x_window_size)].copy()
			y_window_data = data[(i+x_window_size+y_window_size-1):(i+x_window_size+y_window_size)].copy()
			############
			#x_window_data_normalized = x_window_data.loc[:,normalised_cols]
			#x_window_data = x_window_data.drop(x_window_data.loc[:,normalised_cols],axis=1)
			###############
			#Remove any windows that contain NaN
			if(x_window_data.isnull().values.any() or y_window_data.isnull().values.any()):
				i += 1
				continue

			if mean_std:
				min_max = self.obtain_mean_std(x_window_data)
				x_window_data = self.mean_std_standardise(x_window_data, min_max[0], min_max[1])
				y_window_data = self.mean_std_standardise(y_window_data, min_max[0], min_max[1])
				min_max_av = [min_max[0][y_col], min_max[1][y_col]]

			elif zero_std:
				abs_base, x_window_data = self.zero_base_standardise(x_window_data)
				_, y_window_data = self.zero_base_standardise(y_window_data, abs_base=abs_base)
				min_max_av = [1, abs_base[y_col]]

			elif min_max_norm:
				min_max = self.obtain_min_max(x_window_data)
				x_window_data = self.min_max_normalise(x_window_data, min_max[0], min_max[1])
				y_window_data = self.min_max_normalise(y_window_data, min_max[0], min_max[1])
				min_max_av = [min_max[0][y_col], min_max[1][y_col]]


			#Average of the desired predicter y column
			x_data.append(x_window_data.values)
			y_window_data = y_window_data[y_col]
			y_window_data = y_window_data.reindex_axis(sorted(y_window_data.columns), axis=1)
			y_data.append(y_window_data.values)
			min_max_data.append(min_max_av)
			i += 1



			#Restrict yielding until we have enough in our batch. Then clear x, y data for next batch
			if(i % batch_size == 0): #chunks of windows size rows
				#Convert from list to 3 dimensional numpy array [windows, window_val, val_dimension]
				x_np_arr = np.array(x_data)
				#print(len(x_data))
				y_np_arr = np.array(y_data)
				min_max_np_arr = np.array(min_max_data)
				x_data = []
				y_data = []
				min_max_data = []
				yield (x_np_arr, y_np_arr, min_max_np_arr)

			elif ((i+x_window_size+y_window_size) > num_rows and len(x_data)>0):

				x_np_arr = np.array(x_data)
				#print(len(x_data))
				y_np_arr = np.array(y_data)
				min_max_np_arr = np.array(min_max_data)
				x_data = []
				y_data = []
				min_max_data = []
				yield (x_np_arr, y_np_arr, min_max_np_arr)

	def obtain_min_max(self, data):
		#data_min = data[data['Volume']>0].min()
		col = 'Volume'
		for column in data.columns:
			if(column.find('Volume') != -1):
				col = column
				break
		data_min = data[data[col]>0].min()
		data_max = data.max()
		return [data_min, data_max]

	def obtain_mean_std(self, data):
		mean = np.mean(data)
		std = np.std(data)
		#print(mean, std)
		return [mean, std]

	def mean_std_standardise(self, data, mean, std):
		data_standardised = (data - mean) / std
		#print (data_standardised)
		return data_standardised


	def min_max_normalise(self, data, data_min, data_max):
		"""Normalise a Pandas dataframe using column-wise min-max normalisation (can use custom min, max if desired)"""
		#if(data_min.empty): data_min = data[data['Volume']>0].min()
		#if(data_min.empty): data_min = data[data['Volume']>0].min()
		#if(data_max.empty): data_max = data.max()
		data_normalised = ((data-data_min)/(data_max-data_min))#* (2)-1

		#data_normalised = (data_normalised * 2) -1
		return (data_normalised)

	def zero_base_standardise(self, data, abs_base=pd.DataFrame()):
		"""Standardise dataframe to be zero based percentage returns from i=0"""
		if(abs_base.empty): abs_base = data.iloc[0]
		data_standardised = (data/abs_base)-1
		return (abs_base, data_standardised)

