import os
import time
import json
import warnings
import numpy as np
from numpy import newaxis
from keras import regularizers, optimizers
from keras.regularizers import l2
from keras.initializers import TruncatedNormal
from keras.layers import Conv1D, Flatten, Dense, Input, Add, Activation, Multiply
from keras.layers.normalization import BatchNormalization
from keras.layers.core import Dense, Activation, Dropout, Flatten
from keras.layers.recurrent import LSTM
from keras.models import Model, Sequential
from keras.models import load_model
from keras.layers.advanced_activations import LeakyReLU, ELU, PReLU
from keras import backend as K
#configs = json.loads(open(os.path.join(os.path.dirname(__file__), 'config_1d_bitcoin.json')).read())
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

def custom_loss(y_true, y_pred):
    alpha = 100.
    loss = K.switch(K.less(y_true * y_pred, 0), \
        alpha*y_pred**2 - K.sign(y_true)*y_pred + K.abs(y_true), \
        K.abs(y_true - y_pred)
        )
    return K.mean(loss, axis=-1)

dilation_channels = 64
causal_channels = 32
dilation_kernel = 2

def residual_block(X, dilation_rate):
    F = Conv1D(dilation_channels, dilation_kernel, padding='causal', dilation_rate=dilation_rate,
        kernel_regularizer=l2(0.001))(X)
    G = Conv1D(dilation_channels, dilation_kernel, padding='causal', dilation_rate=dilation_rate,
        kernel_regularizer=l2(0.001))(X)
    F = Activation('tanh')(F)
    G = Activation('sigmoid')(G)
    Y = Multiply()([F, G])
    return Y

causal_kernel = 2
causal_channels = 32
dilation_rate = 2
quantization_channels = 256

def bmodel(input_shape, output):
    print(output)
    X_input = Input(input_shape)
    X = Conv1D(causal_channels, causal_kernel, padding='causal', dilation_rate=1)(X_input)
    Y = residual_block(X, 1)
    S0 = Conv1D(dilation_channels, 1, padding="same")(Y)
    Y = Conv1D(causal_channels, 1, padding="same")(Y)
    X = Add()([X, Y])
    Y = residual_block(X, 2)
    S1 = Conv1D(dilation_channels, 1, padding="same")(Y)
    Y = Conv1D(causal_channels, 1, padding="same")(Y)
    X = Add()([X, Y])
    Y = residual_block(X, 4)
    S2 = Conv1D(dilation_channels, 1, padding="same")(Y)
    Y = Conv1D(causal_channels, 1, padding="same")(Y)
    X = Add()([X, Y])
    Y = residual_block(X, 8)
    S3 = Conv1D(dilation_channels, 1, padding="same")(Y)
    Y = Conv1D(causal_channels, 1, padding="same")(Y)
    X = Add()([X, Y])
    Y = residual_block(X, 16)
    S4 = Conv1D(dilation_channels, 1, padding="same")(Y)
    Y = Conv1D(causal_channels, 1, padding="same")(Y)
    X = Add()([X, Y])
    Y = residual_block(X, 32)
    S5 = Conv1D(dilation_channels, 1, padding="same")(Y)
    Y = Conv1D(causal_channels, 1, padding="same")(Y)
    X = Add()([X, Y])
    Y = residual_block(X, 64)
    S6 = Conv1D(dilation_channels, 1, padding="same")(Y)
    Y = Conv1D(causal_channels, 1, padding="same")(Y)
    X = Add()([X, Y])
    Y = residual_block(X, 128)
    S7 = Conv1D(dilation_channels, 1, padding="same")(Y)
    Y = Conv1D(causal_channels, 1, padding="same")(Y)
    X = Add()([X, Y])
    Y = residual_block(X, 256)
    S8 = Conv1D(dilation_channels, 1, padding="same")(Y)
    Y = Conv1D(causal_channels, 1, padding="same")(Y)
    X = Add()([X, Y])
    Y = residual_block(X, 512)
    S9 = Conv1D(dilation_channels, 1, padding="same")(Y)
    S = Add()([S0, S1, S2, S3, S4, S5, S6, S7, S8, S9])
    X = Activation('relu')(S)
    X = Conv1D(128, 1, padding="same")(X)
    X = Activation('relu')(X)
    X = Dropout(0.1)(X)
    X = Conv1D(256, 1, padding="same")(X)
    X = Activation('softmax')(X)
    X = Dropout(0.25)(X)
    X = Flatten()(X)
    X = Dense(512)(X)
    X = PReLU()(X)#LeakyReLU()(X)
    X = Dropout(0.2)(X)
    X = Dense(32)(X)
    X = PReLU()(X)#LeakyReLU()(X)
    X = Dropout(0.15)(X)
    X = Dense(output,activation = 'tanh')(X)

    return Model(inputs = X_input, outputs = X)

def build_network(layers):
    #model = Sequential()
    model = bmodel([layers[1],layers[0]], layers[2])

    '''
    model.add(LSTM(512,return_sequences=True,
        input_shape=(layers[1], layers[0])
        ))
    model.add(BatchNormalization())
    #model.add(LSTM(512
    #    ))
    #model.add(BatchNormalization())
    model.add(Flatten())
    model.add(Dense(units=256
        #,activity_regularizer=regularizers.l2(0.01)
        ))
    model.add(PReLU())
    model.add(BatchNormalization())
    model.add(Dropout(0.1))
    model.add(Dense(units=256
        #,activity_regularizer=regularizers.l2(0.01)
        ))
    model.add(PReLU())
    model.add(BatchNormalization())
    model.add(Dropout(0.1))
    model.add(Dense(units=128
        #,activity_regularizer=regularizers.l2(0.01)
        ))
    model.add(PReLU())
    model.add(BatchNormalization())
    model.add(Dropout(0.05))
    model.add(Dense(units=64
        #,activity_regularizer=regularizers.l2(0.01)
        ))
    model.add(PReLU())
    model.add(BatchNormalization())
    model.add(Dropout(0.05))
    model.add(Dense(units=32
        #,activity_regularizer=regularizers.l2(0.01)
        ))
    model.add(PReLU())
    model.add(BatchNormalization())
    model.add(Dropout(0.05))
    model.add(Dense(units=16
        #,activity_regularizer=regularizers.l2(0.01)
        ))
    model.add(PReLU())
    model.add(BatchNormalization())
    model.add(Dropout(0.05))
    model.add(Dense(units=layers[2]))
    #model.add(LeakyReLU())
    model.add(Activation("tanh"))
    '''
    '''
    model.add(Convolution1D(input_shape = (layers[1], layers[0]),
                        nb_filter=256,
                        filter_length=4,
                        border_mode='same'))

    model.add(BatchNormalization())
    model.add(LeakyReLU())
    model.add(Dropout(0.5))

    model.add(Convolution1D(nb_filter=512,
                        filter_length=4,
                        border_mode='same'))
    model.add(BatchNormalization())
    model.add(LeakyReLU())
    model.add(Dropout(0.5))

    model.add(Convolution1D(nb_filter=512,
                        filter_length=4,
                        border_mode='same'))
    model.add(BatchNormalization())
    model.add(LeakyReLU())
    model.add(Dropout(0.5))
    model.add(Convolution1D(nb_filter=1024,
                        filter_length=16,
                        border_mode='same'))
    model.add(BatchNormalization())
    model.add(Convolution1D(nb_filter=32,
                        filter_length=4,
                        border_mode='same'))
    model.add(BatchNormalization())
    model.add(LeakyReLU())
    model.add(Dropout(0.5))

    model.add(Flatten())

    model.add(Dense(units=512
        #,activity_regularizer=regularizers.l2(0.01)
        ))

    model.add(BatchNormalization())
    model.add(LeakyReLU())
    model.add(Dropout(0.5))
    model.add(Dense(units=128
        #,activity_regularizer=regularizers.l2(0.01)
        ))

    model.add(BatchNormalization())
    model.add(LeakyReLU())
    model.add(Dropout(0.5))
    model.add(Dense(units=layers[2]))
    #model.add(LeakyReLU())
    model.add(Activation("tanh"))
    '''
    

    start = time.time()
    model.compile(
        loss='mse',#configs['model']['loss_function'],
        optimizer='Nadam',#configs['model']['optimiser_function'],
        metrics=["accuracy"])
    #model.summary()

    print("> Compilation Time : ", time.time() - start)
    return model

def load_network(filename):
    #Load the h5 saved model and weights
    if(os.path.isfile(filename)):
        return load_model(filename)#,custom_objects={'custom_loss': custom_loss})
    else:
        print('ERROR: "' + filename + '" file does not exist as a h5 model')
    return None

