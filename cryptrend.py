import gc
import time
import numpy as np
import pandas as pd
import h5py
import matplotlib.pyplot as plt
import json
import iamodel
from keras.callbacks import History, ModelCheckpoint,ReduceLROnPlateau
import dataset_cleaner
from keras import backend as K
from crypto import Crypto
from sgd import CyclicLR, SGDRScheduler

dc = dataset_cleaner.DatasetClean()
K.get_session().list_devices()

class Trainer:

    def __init__(self,jsonfile, val, predictor = False):
        self.configs = json.loads(open(jsonfile).read())
        self.val = val
        self.predictor = predictor
        self.__reload_configs(self.predictor)


    def create_h5_file(self):

        dc.create_clean_datafile(
            filename_in = self.configs['data']['filename'],
            filename_out = self.configs['data']['filename_clean'],
            batch_size = self.configs['data']['gen_size'],
            x_window_size = self.configs['data']['x_window_size'],
            y_window_size = self.configs['data']['y_window_size'],
            y_col = self.configs['data']['y_predict_column'],
            filter_cols = self.configs['data']['filter_columns'],
            normalise = True)

    def __reload_configs(self, predictor = False):
         
        val_fc = 1 if predictor else self.val
        fc = self.configs['data']['filename_clean'].split(".h5")
        fc[0] = fc[0][:12]+f'{val_fc}/'+fc[0][12:]
        self.configs['data']['filename_clean'] = fc[0] +f'_{val_fc}.h5'
    
        fm = self.configs['model']['filename_model'].split(".h5")
        fp = self.configs['model']['filename_predictions'].split(".h5")
        fm[0] = fm[0][:12]+f'{self.val}/'+fm[0][12:]
        fp[0] = fp[0][:12]+f'{self.val}/'+fp[0][12:]
        self.configs['model']['filename_model'] = fm[0] +f'_{self.val}.h5'
        self.configs['model']['filename_predictions'] = fp[0] +f'_{self.val}.h5'
        self.configs['data']['y_window_size'] = self.val

    def data_generator(self, train = False, val = False, test = False):
        data_gen = dc.generate_clean_data(
            self.configs['data']['filename_clean'],
            batch_size = self.configs['data']['batch_size'],
            split = self.configs['data']['split'],
            start_index = self.configs['data']['start_index'],
            train = train,
            val = val,
            test = test)
        return data_gen
    
    def fit_crypto(self, save_period = 1, load = False):
        #period: checkpoitn to save time
        
        checkpoint = ModelCheckpoint(self.configs['model']['filename_model'], monitor='val_loss', verbose=1, save_best_only=True, save_weights_only=False, mode='auto', period=save_period)
        reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.9, patience=2, min_lr=0.000001, verbose=1)

        with h5py.File(self.configs['data']['filename_clean'], 'r') as hf:
            nrows = hf['x'].shape[0] - self.configs['data']['start_index']
            ncols = hf['x'].shape[2]
            noutputs = hf['y'].shape[2]
            print("x",hf['x'],"\ny",hf['y'])
    
        ntrain = int(self.configs['data']['split'] * nrows)
        ntest = int(nrows - ntrain)
        steps_val = int(ntest/self.configs['data']['batch_size'])
        steps_per_epoch =int(ntrain /self.configs['data']['batch_size'])

        scheduleCyclic = CyclicLR(base_lr=0.0001, max_lr=0.0008,
                                step_size=40, mode='triangular')

        schedule = SGDRScheduler(min_lr=2e-4,
                                     max_lr=3e-3,
                                     steps_per_epoch=steps_per_epoch,
                                     lr_decay=0.9,
                                     cycle_length=50,
                                     mult_factor=1.1)

        callbacks_list = [checkpoint,reduce_lr]
        if steps_val < 1:
            steps_val = 1 
        if steps_per_epoch < 1:
            steps_per_epoch = 1 
    
        print('> Clean data has', nrows, 'data rows. Training on', ntrain, 
                'rows with', steps_per_epoch, 'steps-per-epoch', 'Test on', ntest,
                'rows with', steps_val, 'steps')

        if load:
            model = iamodel.load_network(self.configs['model']['filename_model'])
        else:
            model = iamodel.build_network([ncols, self.configs['data']['x_window_size'], noutputs])
        data_gen_train = self.data_generator(train = True)
        data_gen_val = self.data_generator(val = True)

        self.history = self.__fit_model_threaded(model, data_gen_train, 
            steps_per_epoch, data_gen_val, steps_val, callbacks_list)
        #del self.history # comment if you want to plot
        del data_gen_val
        del data_gen_train
    
    def __fit_model_threaded(self, model, data_gen_train, steps_per_epoch, data_gen_val, steps_val, callbacks_list):
        h = model.fit_generator(
        self.__generator_strip_xymm(data_gen_train),
        steps_per_epoch = steps_per_epoch,
        epochs = self.configs['model']['epochs'],
        shuffle = True,
        verbose = 1,
        validation_data = self.__generator_strip_xymm(data_gen_val),
        validation_steps = steps_val,
        callbacks = callbacks_list
        )
        model.save(self.configs['model']['filename_model'])
        print('> Model Trained! Weights saved in', self.configs['model']['filename_model'])
        del model
        return h

    def plot_history(self):
        print(self.history.history.keys())
        # summarize history for loss
        plt.plot(self.history.history['loss'])
        plt.plot(self.history.history['val_loss'])
        plt.title('model loss')
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['train', 'test'], loc='upper left')
        plt.show()
        # summarize history for accuracy
        plt.plot(self.history.history['acc'])
        plt.plot(self.history.history['val_acc'])
        plt.title('model accuracy')
        plt.ylabel('accuracy')
        plt.xlabel('epoch')
        plt.legend(['train', 'test'], loc='upper left')
        plt.show()
        del(self.history)

    def clean(self):
        gc.collect()
        K.clear_session()



    def __generator_strip_xymm(self,data_gen): 
        while True:
            x,y,mm=next(data_gen)
            yield (x,np.squeeze(y))


class Predictor(Trainer):

    def __init__(self,jsonfile, val, to_predict):
        self.val = val
        super().__init__(jsonfile, val, predictor = True)
        self.model = iamodel.load_network(self.configs['model']['filename_model'])
        self.to_predict = sorted(self.configs['data']['y_predict_column']).index(to_predict)


    def one_day_prediction(self, plot=True):
        true_values = []
        min_max_values = []
        data_gen_val = self.data_generator(test = True)
        with h5py.File(self.configs['data']['filename_clean'], 'r') as hf:
            nrows = hf['x'].shape[0] - self.configs['data']['start_index']

        ntrain = int(self.configs['data']['split'] * nrows)
        ntest = int(nrows - ntrain)
        #print(ntrain, nrows)
        steps_test = int(ntest/self.configs['data']['batch_size'])

        if steps_test < 1:
            steps_test = 1

        print('> Testing model on', ntest, 'data rows with', steps_test, 'steps')

        predictions = self.model.predict_generator(
            self.__generator_strip_xy(data_gen_val, true_values, min_max_values),
            steps = 1,#steps_test,
            verbose = 1
        )
        true_values = np.array(true_values[:len(predictions)])
        min_max = np.array(list(zip(*min_max_values[:len(predictions)])))
        true_values = self.__denormalization(true_values[:,0], min_max)
        predictions = self.__denormalization(predictions, min_max)
        #Save our predictions
        with h5py.File(self.configs['model']['filename_predictions'], 'w') as hf:
            print('> Saving predictions on', self.configs['model']['filename_predictions'] )
            dset_p = hf.create_dataset('predictions', data=predictions)
            dset_y = hf.create_dataset('true_values', data=true_values)

        self.__plot_results(predictions[:,self.to_predict], true_values[:,self.to_predict].reshape(-1))
        return(predictions, true_values)
        #del data_gen_val
        #del predictions


    def __generator_strip_xy(self,data_gen, true_values, min_max_values): 
        while True:
            x,y,min_max = next(data_gen)
            #print(x.shape)
            true_values += list(y)
            min_max_values += list(min_max)
            yield x


    def __plot_results(self, predicted_data, true_data):
        fig=plt.figure(figsize=(18, 12), dpi= 80, facecolor='w', edgecolor='k')
        ax = fig.add_subplot(111)
        ax.plot(true_data, label='True Data')
        plt.plot(predicted_data, label='Prediction')
        plt.legend()
        plt.show()

    def multiple_days_predict(self, days=15,batch_size=1000):
        #Reload the data-generator
        data_gen_test = self.__data_batch_generator(test = True, batch_size=batch_size)
        data_x, true_values, min_max = next(data_gen_test)
        window_size = days #number of steps to predict into the future
        predictions_multiple = self.__predict_sequences_multiple(
            self.model,
            data_x,
            data_x[0].shape[0],
            window_size
        )
        #seleccionamos columna1
        self.__plot_results_multiple(predictions_multiple[:,:,self.to_predict], true_values[:,:,self.to_predict].reshape(-1), window_size)
        min_max = np.array(list(zip(*min_max)))
        true_values = self.__denormalization(true_values[:,0], min_max)
        predictions_multiple = predictions_multiple.reshape(predictions_multiple.shape[0] * 
            predictions_multiple.shape[1],-1)
        predictions_multiple = self.__denormalization(predictions_multiple, self.__min_max_window(min_max, days))#[:,:len(predictions_multiple)])
        self.__graph_results_multiple(predictions_multiple[:,self.to_predict], true_values[:,self.to_predict], days)
        del data_gen_test
        del predictions_multiple
        del true_values

    def __data_batch_generator(self, train = False, val = False, test = False, batch_size=1000):
        data_gen = dc.generate_clean_data(
            self.configs['data']['filename_clean'],
            batch_size = batch_size,
            split = self.configs['data']['split'],
            start_index = self.configs['data']['start_index'],
            train = train,
            val = val,
            test = test
        )
        return data_gen

    def __predict_sequences_multiple(self, model, data, window_size, prediction_len):
        prediction_seqs = []
        print(len(data)/prediction_len)
        for i in range(int(len(data)/prediction_len)):
            curr_frame = data[i*prediction_len]
            predicted = []
            for j in range(prediction_len):
                predicted.append(model.predict(curr_frame[np.newaxis,:,:]))
                #print(curr_frame[1:], curr_frame[np.newaxis,:,:])
                curr_frame = curr_frame[1:]
                curr_frame = np.insert(curr_frame, [window_size-1], predicted[-1], axis=0)
            prediction_seqs.append(predicted)
        return (np.array(prediction_seqs)[:,:,0])

    def __predict_sequences_multiple_day(self, model, data, window_size, prediction_len):
        prediction_seqs = []
        print(len(data)/prediction_len)
        for i in range(int(len(data))):
            curr_frame = data[i]
            predicted = []
            #print(type(curr_frame))
            for j in range(prediction_len):
                predicted.append(model.predict(curr_frame[np.newaxis,:,:])[0,0])
                #print(curr_frame[1:], curr_frame[np.newaxis,:,:])
                curr_frame = curr_frame[1:]
                curr_frame = np.insert(curr_frame, [window_size-1], predicted[-1], axis=0)
                #print(curr_frame)
            prediction_seqs.append(predicted)
        return (prediction_seqs)

    def __plot_results_multiple(self, predicted_data, true_data, prediction_len):
        fig=plt.figure(figsize=(18, 12), dpi= 80, facecolor='w', edgecolor='k')
        ax = fig.add_subplot(111)
        ax.plot(true_data, label='True Data')
        #Pad the list of predictions to shift it in the graph to it's correct start
        for i, data in enumerate(predicted_data):
            padding = [None for p in range(i * prediction_len)]
            #print(padding+data)
            plt.plot(padding + list(data), label='Prediction')
            plt.legend()
        plt.show()

    def __graph_results_multiple(self, predicted_data, true_data, prediction_len):
        fig=plt.figure(figsize=(18, 12), dpi= 80, facecolor='w', edgecolor='k')
        ax = fig.add_subplot(111)
        ax.plot(true_data, label='True Data')
        #plt.plot(predicted_data, label='Prediction')
        #Pad the list of predictions to shift it in the graph to it's correct start
        for i in range(len(predicted_data) // prediction_len):
            padding = [None for p in range((i) * prediction_len)]
            x = predicted_data[i * prediction_len:(i+1) * prediction_len]
            #y = np.arange(i*prediction_len,(i+1) * prediction_len)
            plt.plot(padding + list(x),label='Prediction')    
        plt.show()

    def clean(self):
        del self.model
        gc.collect()
        K.clear_session()

    def __min_max_window(self, min_max, window_size):        
        data = []
        for i in range(len(min_max[0]) // window_size):
            data = data + ([min_max[:,i * window_size]] * window_size)
        data = np.array(data)
        data = np.array([data[:,0],data[:,1]])
        return data

    def __denormalization(self, values, min_max):
        #data = values * min_max[1] + min_max[0] #mean_std
        #data = ((min_max[1] - min_max[0]) * (values+1))/2+ min_max[0]#minmax1
        data =(min_max[1]-min_max[0])*values + min_max[0]#min_max0
        #data = (values + 1) * min_max[1]#stdrize
        return data 

