### TODO SE QUEDA  IGUAL para las tablas lo unico que cambia es la parte de la clase predictor 
## el retorno de la funcion predict es un dataframe de pandas al que puedes acceder con el nombre de la columna
## por ejemplo predictions['Close_bt'] eso te va a entregar otro dataframe, pero a ti te interesa solo el arreglo
## al arreglo entras como predictions['Close_bt'].values y asi te regresa los valores para que los grafiques
## si quieres imprimir el dataframe para ver que tiene solo pon print(predictions.head(15))
##  si quieres saber que columnas hay es predictions.columns
## y podrías iterar sobre ellas con un for col in predictions.columns:
from predictor import Cryptopredict as cp

ethereum_5m = cp('ethereum', config_file = 'config_5min_ethereum.json')
models = ethereum_5m.load_models()
predictions = ethereum_5m.predict(models)

bitcoin_5m = cp('bitcoin', config_file = 'config_5min_bitcoin.json')
models = bitcoin_5m.load_models()
predictions = bitcoin_5m.predict(models)

bt_eth_5m = cp(config_file = 'config_5min.json')
models = bt_eth_5m.load_models()
predictions = bt_eth_5m.predict(models)

ethereum_1d = cp('ethereum', config_file = 'config_1d_ethereum.json')
models = ethereum_1d.load_models()
predictions = ethereum_1d.predict(models)

bitcoin_1d = cp('bitcoin', config_file = 'config_1d_bitcoin.json')
models = bitcoin_1d.load_models()
predictions = bitcoin_1d.predict(models)

bt_eth_1d = cp(config_file = 'config_1d.json')
models = bt_eth_1d.load_models()
predictions = bt_eth_1d.predict(models)
#######################################################
from crypto import Crypto, Exchanges

bitcoin = Crypto('bitcoin') #Init Class
bitcoin.sent_data_clean()   #Clean CSV sentiment
bit = bitcoin.merge_tables()      #Merge sentiment + OHLCV tables

ethereum = Crypto('ethereum')
ethereum.sent_data_clean()
eth = ethereum.merge_tables()
Exchanges(bit,'bt', eth, 'eth').merge()
#######################################################
