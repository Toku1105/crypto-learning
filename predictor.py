import numpy as np 
import time
import pandas as pd
import tensorflow as tf
import matplotlib
import matplotlib.pyplot as plt
from dateutil.parser import parse
import datetime
from datetime import timedelta
import json
import collections
import os
import ccxt 
from keras.models import load_model
class Cryptopredict:

    def __init__(self, sym = 'bitcoin', config_file='config.json'):
        self.configs = json.loads(open(os.path.join(os.path.dirname(__file__), config_file)).read())
        self.timeframe = self.configs['data']['timeframe']
        self.syms = ['bitcoin', 'ethereum']
        #self.syms = [sym] if self.timeframe == '5m' else ['bitcoin','ethereum']

    def __clean_data(self, df):

        filter_cols = self.configs['data']['filter_columns']
        if(filter_cols):
            rm_cols = set(df.columns) - set(filter_cols)
            for col in rm_cols:
                del df[col]
        df = df[-self.configs['data']['x_window_size']:]
        df = df.reindex_axis(sorted(df.columns), axis=1)
        min_max = self.__obtain_min_max(df)
        df = self.__min_max_normalise(df, min_max[0], min_max[1])
        return df, min_max

    def __obtain_min_max(self, data):
        data_min = data.min()
        data_max = data.max()
        return [data_min, data_max]

    def __min_max_normalise(self, data, data_min, data_max):
        data_normalised = ((data-data_min)/(data_max-data_min))
        return (data_normalised)

    def __add_volatility(self, data):
        kwargs = {'Close_off_high': lambda x: 2*(x['High']- x['Close'])/(x['High']-x['Low'])-1,
        'Volatility_HLO': lambda x: (x['High']- x['Low'])/(x['Open']),
        'Volatility': lambda x: x['Close'].rolling(self.configs['data']['x_window_size']).std(),
        'Mean': lambda x: x['Close'].rolling(self.configs['data']['x_window_size']).mean()}
        data = data.assign(**kwargs).fillna(0)
        return data


    def __fetch_data(self, sym):
        if sym == 'bitcoin':
            symbol = 'BTC/USDT'
        elif sym == 'ethereum':
            symbol = 'ETH/USDT'
        days = 2 if self.timeframe == '5m' else 300
        exchange = ccxt.bitfinex()
        fromTime = datetime.datetime.utcnow() - timedelta(days = days)
        fromTime = exchange.parse8601(fromTime.strftime("%Y-%m-%d %H:%M:%S"))

        ohlcv = collections.OrderedDict()
        h = dict()

        if(exchange.has['fetchOHLCV']):
            ohlcv.clear()
            data = exchange.fetch_ohlcv(symbol, self.timeframe, fromTime, limit = 1000)
            data = list(zip(*data))
            data[0] = [datetime.datetime.utcfromtimestamp(ms / 1000) for ms in data[0]]
            ohlcv['Date'] = data[0]
            ohlcv['Open'] = data[1]
            ohlcv['High'] = data[2]
            ohlcv['Low'] = data[3]
            ohlcv['Close'] = data[4]
            ohlcv['Volume'] = data[5]
            df = pd.DataFrame(ohlcv)
            df.set_index('Date', inplace=True)
            df = df.loc[df['Volume']!=0]
            if(self.timeframe == '1d'):
                df = self.__add_sentiment(df, sym)

            df = self.__add_volatility(df)
            
            df = df.loc[df['Volatility']!=0]
            
            return df

    def __add_sentiment(self, df, symbol):
        sentiment_table = pd.read_csv('./databases/'+symbol+'_sent.csv', index_col=0)
        sequence_cols = ['Sentiment']#'High_Sent','Low_Sent']
        sentiment_table = sentiment_table[sequence_cols]
        df = pd.merge(sentiment_table, df, how = 'inner',left_index=True, right_index=True)
        return df

    def __filename(self, val):
         
        fm = self.configs['model']['filename_model'].split(".h5")
        fm[0] = fm[0][:12]+f'{val}/'+fm[0][12:]
        fm = fm[0] +f'_{val}.h5'
        return fm

    def __multiple_predictions(self,data, model, prediction_len):
        predicted = {'Model':[], 'Date':[]}
        curr_frame = data
        for j in range(prediction_len):
            
            predicted['Model'].append(model.predict(curr_frame[np.newaxis,:,:]))
            predicted['Date'].append((self.lastDate + self.timed * (j + 1)).isoformat(' '))
            curr_frame = curr_frame[1:]
            curr_frame = np.insert(curr_frame, [data.shape[0]-1], predicted['Model'][-1], axis=0)
        return (predicted)  

    def __denormalization2(self, values, min_max):
        prediction_col = self.configs['data']['y_predict_column']
        data =(min_max[1][prediction_col] - min_max[0][prediction_col]) * values + min_max[0][prediction_col]
        return data 
    
    def __denormalization(self, values, min_max):
        #data = values * min_max[1] + min_max[0] #mean_std
        #data = ((min_max[1] - min_max[0]) * (values+1))/2+ min_max[0]#minmax1
        data =(min_max[1]-min_max[0])*values + min_max[0]#min_max0
        #data = (values + 1) * min_max[1]#stdrize
        return data 

    def load_models(self, prediction_len = 1):
        models = []
        for i in range(1, prediction_len + 1, 1):
            models.append(self.load_network(self.__filename(i)))
        return models

    def predict(self, models):
        output = pd.DataFrame()
        df_list = []
        for sym in self.syms:
            df_list.append(self.__fetch_data(sym=sym))
        df = df_list[0] if (len(df_list) < 2) else pd.merge(df_list[0], df_list[1], how = 'inner',
                                                    suffixes=['_'+'bt','_'+'eth'],
                                                    left_index=True, right_index=True)
        df, min_max = self.__clean_data(df)
        df = df.reindex_axis(sorted(df.columns), axis=1)
        #print(df.head())
        #print(df.tail())
        self.lastDate = df.index[-1]
        self.timed = timedelta(minutes = 5) if self.timeframe == '5m' else timedelta(days = 1)
        val = df.values
        #val = np.array(list(zip(*df.values))).T
        #predictions = {'Model': [], 'Date':[]}
        predictions_seq = {}
        y_list = self.configs['data']['y_predict_column']
        i = 1
        for model in models:
            #predictions['Model'].append(model.predict(val[np.newaxis,:,:])[0,0])
            #predictions['Date'].append((self.lastDate + self.timed * i).isoformat(' '))
            if(i == 1):
                predictions_seq = self.__multiple_predictions(val, model, 15)
            i = i + 1
        predictions = np.squeeze(np.array(predictions_seq['Model']))
        output['Date'] = predictions_seq['Date']
        output.set_index('Date', inplace = True)
        for j in range(predictions.shape[1]):
            output[y_list[j]] = predictions[:,j]
        output = self.__denormalization(output, min_max)
        #predictions_seq['Model'] = self.__denormalization(np.array(predictions_seq['Model']), self.min_max)
        return output

    def load_network(self, filename):
        #Load the h5 saved model and weights
        if(os.path.isfile(filename)):
            model = load_model(filename)#,custom_objects={'custom_loss': custom_loss})
            model._make_predict_function()
            return model
        else:
            print('ERROR: "' + filename + '" file does not exist as a h5 model')
        return None


    def debug(self):
        df_list = []
        for sym in self.syms:
            df_list.append(self.__fetch_data(sym=sym))
        df = df_list[0] if (len(df_list) < 2) else pd.merge(df_list[0], df_list[1], how = 'inner',
                                                    suffixes=['_'+'bt','_'+'eth'],
                                                    left_index=True, right_index=True)
        df = self.__clean_data(df)

        print(df.head().values)